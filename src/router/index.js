import Vue from 'vue'
import VueRouter from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login'
Vue.use(VueRouter)


import store from '@/store'


const routes = [{
    path: '/',
    name: 'Login',
    component: Login,
    meta: {
      requiresAuth: false,
    },
  },

  {
    path: '/imgs',
    name: 'HelloWorld',
    component: HelloWorld,
    meta: {
      requiresAuth: true,
    },


  }

];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


router.beforeEach((to, from, next) => {
  // if (to.matched.some(record => record.meta.requiresAuth)) {
  if (to.meta.requiresAuth) {
    console.log(to.meta.requiresAuth)
    if (!store.getters.getAuth) {
      next({
        path: '/',
        params: {
          nextUrl: to.fullPath
        }
      })
    } else {
      next()
    }
  } else {
    if (store.getters.getAuth) {
      next({
        path: '/imgs',
        params: {
          nextUrl: '/'
        }
      })
    } else {
      next()
    }
  }
})

// router.beforeEach((to, from, next) => {

//   if (to.meta.requiresAuth) {
//     if (store.getters.getAuth) {
//       next('/imgs')
//     } else {
//       next('/')
//     }
//   } else {
//     next();
//   }

// })

export default router;