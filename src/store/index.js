import 'es6-promise/auto'
import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({

  state: {
    auth: false,

  },

  getters: {
    getAuth: state => {
      return state.auth
    }
  },

  mutations: {
    setAuth(state, pass) {

      if (pass === process.env.VUE_APP_PASSVALIDATE) {
        state.auth = true
      }

    },
  },
  actions: {

    loginauth(context, pass) {
      context.commit('setAuth', pass)
    },

  }

})